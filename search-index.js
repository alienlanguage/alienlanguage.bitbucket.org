var searchIndex = {};
searchIndex["alienlanguage"] = {"doc":"A crate for matching patterns from a noisy signal against a dictionary of known words from an [alien language][1].","items":[[3,"Problem","alienlanguage","Represents an alien language problem set (sample, small, or large).",null,null],[11,"from_lines","","Constructs a new `Problem` with the specified input lines iterator.",0,{"i":[{"n":"iterator"}],"o":{"n":"problem"}}],[11,"next","","",0,{"i":[{"n":"self"}],"o":{"g":["string"],"n":"option"}}]],"paths":[[3,"Problem"]]};
searchIndex["webservice"] = {"doc":"A web service for matching patterns from a noisy signal against a dictionary of known words from an [alien language][1].","items":[],"paths":[]};
initSearch(searchIndex);
